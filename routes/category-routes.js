const Router = require("express").Router();
const { all, find, create } = require("../controllers/CategoryController");

/** @todo need to refactor */
const ModelDependencyInjection = async (req, res, next) => {
  const params = req.params;
  const rawModels = Object.keys(params);

  for (const rawModel of rawModels) {
    const capitalizeModel = rawModel.replace(/^[a-z]/, (str) =>
      str.toUpperCase()
    );
    const { [capitalizeModel]: Model } = require("../models");

    if (!Model) {
      res.status(400).json({
        message: `Please provide a proper model name as ${rawModel} is not valid`,
      });
    }

    try {
      const found = await Model.findOne({
        where: {
          uuid: params[rawModel],
        },
      });

      if (!found) {
        res.status(404).json({
          message: `UUID of ${params[rawModel]} not found`,
        });
      }

      req[rawModel] = found;
      next();
    } catch (e) {
      res.status(400).json({
        message: e.message,
      });
    }
  }
};

module.exports = (app) => {
  Router.get("/", all);
  Router.get("/:category", ModelDependencyInjection, find);
  Router.post("/", create);

  app.use("/categories", Router);
};
