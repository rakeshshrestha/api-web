const Router = require("express").Router();
const {
  all,
  find,
  create,
  update,
  delete: deletePost,
} = require("../controllers/PostController");

module.exports = (app) => {
  Router.get("/", all);
  Router.get("/find/:uuid", find);
  Router.post("/create", create);
  Router.put("/update/:uuid", update);
  Router.delete("/delete/:uuid", deletePost);

  app.use("/posts", Router);
};
