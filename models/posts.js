"use strict";
const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class posts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    toJSON() {
      const { id, ...rest } = this.get();
      return rest;
    }
  }
  posts.init(
    {
      uuid: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false,
        notEmpty: true,
      },
      content: {
        type: DataTypes.TEXT,
        allowNull: false,
        // @todo add validator
      },
      readTime: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      publishedDate: {
        type: DataTypes.DATE,
        allowNull: true,
        Default: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "Post",
      underscored: true,
      paranoid: true,
    }
  );
  return posts;
};
