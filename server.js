const express = require("express");
const morgan = require("morgan");

const app = express();

app.use(morgan("tiny"));
app.use(express.json());

require("./routes/post-routes")(app);
require("./routes/category-routes")(app);

app.listen(3000, () => {
  console.log(`Server started at http://localhost:3000`);
});
