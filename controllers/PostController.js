const { Post } = require("../models");

// Get all posts
exports.all = async (req, res) => {
  const posts = await Post.findAll();
  res.status(200).json(posts);
};

// Find a post
exports.find = async (req, res) => {
  const { uuid } = req.params;
  const post = await Post.findOne({ where: { uuid } });
  if (!post) {
    res.status(400).json({ message: "User not found" });
  }
  res.status(200).json({ data: post });
};

/**
 * Create a post
 *
 * @todo add validation with joi
 *
 */
exports.create = async (req, res) => {
  const { title, content, readTime } = req.body;

  const createdPost = await Post.create({ title, content, readTime });
  res
    .status(200)
    .json({ message: "User created successfully", data: createdPost });
};

/**
 *  Update a post
 *
 * @todo add validation with joi
 *
 */
exports.update = async (req, res) => {
  const { title, content, readTime } = req.body;
  const { uuid } = req.params;

  const updatedPost = await Post.update(
    { title, content, readTime },
    {
      where: {
        uuid,
      },
    }
  );
  res
    .status(200)
    .json({ message: "User created successfully", data: req.body });
};

/**
 * Delete a post
 *
 */
exports.delete = async (req, res) => {
  const { uuid } = req.params;
  const post = await Post.findOne({ where: { uuid } });
  post.destroy();
  res.status(200).json({ message: "Post deleted successfully" });
};
