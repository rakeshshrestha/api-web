const { Category } = require("../models");

exports.all = async (req, res) => {
  const categories = await Category.findAll();
  res.json({
    data: categories,
  });
};

exports.find = (req, res) => {
  res.json({ data: req.category });
};

exports.create = async (req, res) => {
  const { title } = req.body;
  const createdCategory = await Category.create({ title });
  res
    .status(201)
    .json({ message: "Category created successfully", data: createdCategory });
};
